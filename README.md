                           **Reach app task**
## Description

Reach contain home screen that has search view , discover sections and bottom navigation.


## technologies i used

1. Kotlin with rxjava.
2. MVVM design pattern.
3. Live data and observer pattern.
4. Retrofit for remote data.
5. Nave Graph with bottom navigation.
6. used ConstraintLayout , card view , relativeLayout in design.
7. used extension functions.
8. Caching by OkHttpClient.
9. unit test by truth library and JUnit4


## Installation

download app from media fire link and install it.


## Usage

Reach app used for discover many categories like restaurants , shopping and  cafes.

