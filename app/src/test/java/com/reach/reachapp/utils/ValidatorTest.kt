package com.reach.reachapp.utils

import org.junit.Test

import com.google.common.truth.Truth.assertThat

class ValidatorTest{
    @Test
    fun searchValidator() {
        val searchKey = "search"
        val result = Validator.searchValidator(searchKey)
        assertThat(result).isTrue()
    }
}