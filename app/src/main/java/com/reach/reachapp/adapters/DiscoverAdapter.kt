package com.reach.reachapp.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.reach.reachapp.R
import com.reach.reachapp.models.categories.CategoriesModel
import com.reach.reachapp.models.categories.Data
import java.util.ArrayList


class DiscoverAdapter(
    var context: Context,
    var categoriesModel: MutableList<Data>
) :
    RecyclerView.Adapter<DiscoverAdapter.ViewHolder>(), Filterable {

    var categoriesModelFilter = ArrayList<Data>()

    init {
        categoriesModelFilter = categoriesModel as ArrayList<Data>
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.discover_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        categoriesModelFilter[position].name?.let {
            holder.discoverName.text = it

        }

        holder.discoverImagesRecycler.adapter =
            DiscoverImagesAdapter(context, categoriesModelFilter[position].user)
    }

    override fun getItemCount(): Int {
        return categoriesModelFilter.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var discoverName: TextView = itemView.findViewById(R.id.discoverName)
        var discoverImagesRecycler: RecyclerView =
            itemView.findViewById(R.id.discoverImagesRecycler)

    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                Log.i("Changed1", charSearch);

                if (charSearch.isEmpty()) {
                    categoriesModelFilter = categoriesModel as ArrayList<Data>
                } else {
                    val resultList = ArrayList<Data>()
                    for (row in categoriesModel) {
                        if (row.name.toLowerCase().contains(constraint.toString().toLowerCase())) {
                            resultList.add(row)
                        }
                    }
                    categoriesModelFilter = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = categoriesModelFilter
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                categoriesModelFilter = results?.values as ArrayList<Data>
                notifyDataSetChanged()
            }
        }
    }
}

