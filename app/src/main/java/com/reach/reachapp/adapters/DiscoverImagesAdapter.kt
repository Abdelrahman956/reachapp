package com.reach.reachapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.reach.reachapp.R
import com.reach.reachapp.utils.loadImage
import com.reach.reachapp.models.categories.User
import de.hdodenhof.circleimageview.CircleImageView


class DiscoverImagesAdapter(var context: Context, var user: User) :
    RecyclerView.Adapter<DiscoverImagesAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.discover_images_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        user.data[position].name?.let {
            holder.name.text = it
        }
        user.data[position].coverPhoto?.let {
            holder.discoverCover.loadImage(it)
        }
        user.data[position].profilePicture?.let {
            holder.profileImage.loadImage(it)
        }
        if (position == user.data.size - 1) {
            holder.viewMore.visibility = View.VISIBLE
        } else {
            holder.viewMore.visibility = View.GONE

        }


    }

    override fun getItemCount(): Int {
        return user.data.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.name)
        var viewMore: TextView = itemView.findViewById(R.id.viewMore)
        var discoverCover: ImageView = itemView.findViewById(R.id.discoverCover)
        var profileImage: CircleImageView = itemView.findViewById(R.id.profileImage)

    }

}

