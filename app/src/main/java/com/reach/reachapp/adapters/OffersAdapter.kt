package com.reach.reachapp.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.reach.reachapp.R
import com.reach.reachapp.utils.loadImage
import com.reach.reachapp.models.offers.OffersModel


class OffersAdapter(var context: Context, var offersModel: OffersModel) :
    RecyclerView.Adapter<OffersAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.offers_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        offersModel.data.offers.data[position].coverImage?.let {

            holder.offersImage.loadImage(it)
        }
        holder.itemView.setOnClickListener(View.OnClickListener {
            val uri: Uri =
                Uri.parse(offersModel.data.offers.data[position].cta_url)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

            context.startActivity(intent)
        })

    }

    override fun getItemCount(): Int {
        return offersModel.data.offers.data.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var offersImage: ImageView = itemView.findViewById(R.id.offersImage)

    }
}

