package com.reach.reachapp.repository

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData

import com.google.gson.Gson
import com.reach.reachapp.models.offers.OffersModel
import com.reach.reachapp.network.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class OffersRepository {
    private var mutableLiveData = MutableLiveData<OffersModel>()

    var context: Context? = null
    var disposable: Disposable? = null

    fun getMutableLiveData(context: Context): MutableLiveData<OffersModel> {

        this.context = context
        mutableLiveData = MutableLiveData()
        getOffers()
        return mutableLiveData
    }

    private fun getOffers() {
        val userDataService = RetrofitClient.getInstance().api

        disposable = userDataService.getOffersList()

            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    mutableLiveData.setValue(result)

                },
                { error ->
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
                }
            )

    }
}