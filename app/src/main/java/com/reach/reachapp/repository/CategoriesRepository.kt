package com.reach.reachapp.repository

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData

import com.google.gson.Gson
import com.reach.reachapp.models.categories.CategoriesModel
import com.reach.reachapp.models.offers.OffersModel
import com.reach.reachapp.network.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class CategoriesRepository {
    private var mutableLiveData = MutableLiveData<CategoriesModel>()

    var context:Context? = null
    var disposable: Disposable? = null

    fun getMutableLiveData(context:Context): MutableLiveData<CategoriesModel> {

        this.context = context
        mutableLiveData = MutableLiveData()
        getCategories()
        return mutableLiveData
    }

    private fun getCategories() {
        val userDataService = RetrofitClient.getInstance().api

        disposable = userDataService.getCategories()

            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    mutableLiveData.setValue(result)

                },
                { error ->
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
                }
            )

    }
}