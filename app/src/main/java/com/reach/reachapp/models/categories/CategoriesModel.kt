package com.reach.reachapp.models.categories

import com.google.gson.annotations.SerializedName

data class CategoriesModel(
    @SerializedName("data")
    var data: List<Data>,
)

data class Data(
    @SerializedName("id")
    val id: Int,
    @SerializedName("slug")
    val slug: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("users")
    val user: User,
)

data class User(
    @SerializedName("data")
    var data: List<UserData>,
)

data class UserData(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("profile_picture")
    val profilePicture: String,
    @SerializedName("cover_photo")
    val coverPhoto: String,

    )