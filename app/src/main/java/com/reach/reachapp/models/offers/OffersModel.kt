package com.reach.reachapp.models.offers

import com.google.gson.annotations.SerializedName

data class OffersModel(

        @SerializedName("data")
        val data: Data,

        )

data class Data(
        @SerializedName("offers")
        val offers: Offers,
)

data class Offers(
        @SerializedName("data")
        val data: List<OffersData>,
)

data class OffersData(
        @SerializedName("id")
        val id: Int,
        @SerializedName("cover_image")
        val coverImage: String,
        @SerializedName("cta_url")
        val cta_url: String,
)
