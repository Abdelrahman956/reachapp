package com.reach.reachapp.viewmodels

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.reach.reachapp.models.offers.OffersModel
import com.reach.reachapp.repository.OffersRepository

class OffersViewModel : ViewModel() {

    private var offersRepository: OffersRepository = OffersRepository()

    fun getOffersList(context: Context): LiveData<OffersModel> {
        return offersRepository.getMutableLiveData(context)
    }


}