package com.reach.reachapp.viewmodels

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.reach.reachapp.models.categories.CategoriesModel
import com.reach.reachapp.repository.CategoriesRepository

class CategoriesViewModel : ViewModel() {

    private var categoriesRepository: CategoriesRepository = CategoriesRepository()

    fun getCategoriesList(context: Context): LiveData<CategoriesModel> {
        return categoriesRepository.getMutableLiveData(context)
    }


}