package com.reach.reachapp.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.reach.reachapp.R
import com.reach.reachapp.adapters.DiscoverAdapter
import com.reach.reachapp.adapters.OffersAdapter
import com.reach.reachapp.models.categories.Data
import com.reach.reachapp.utils.Constants.TAG
import com.reach.reachapp.utils.Validator
import com.reach.reachapp.viewmodels.CategoriesViewModel
import com.reach.reachapp.viewmodels.OffersViewModel
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    var adapter: DiscoverAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val searchTest: EditText = root.findViewById(R.id.search)
        val searchIcon: ImageView = root.findViewById(R.id.searchIcon)
        val offersViewModel = ViewModelProvider(this).get(OffersViewModel::class.java)

        offersViewModel.getOffersList(requireContext()).observe(viewLifecycleOwner, { offersList ->

            val adapter = OffersAdapter(requireContext(), offersList)
            offersRecycler.adapter = adapter
            Log.e(TAG, "" + Gson().toJson(offersList))
        })

        val categoriesViewModel = ViewModelProvider(this).get(CategoriesViewModel::class.java)

        categoriesViewModel.getCategoriesList(requireContext())
            .observe(viewLifecycleOwner, { categoriesList ->

                adapter = DiscoverAdapter(
                    requireContext(),
                    categoriesList.data as MutableList<Data>
                )
                discoverRecycler.adapter = adapter

                Log.e(TAG, "" + Gson().toJson(categoriesList))
            })

        searchIcon.setOnClickListener(View.OnClickListener {
            val result: Boolean = Validator.searchValidator(searchTest.text.toString())

            if (!result) {
                Toast.makeText(activity, "empty field", Toast.LENGTH_SHORT).show()


            } else {
                adapter?.filter?.filter(searchTest.text.toString())
                Log.i("Changed", searchTest.text.toString());

            }
        })

        val tw: TextWatcher = object : TextWatcher {

            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                val result: Boolean = Validator.searchValidator(editable.toString())
                if (!result) {
                    Toast.makeText(activity, "empty field", Toast.LENGTH_SHORT).show()


                } else {
                    adapter?.filter?.filter(editable.toString())
                    Log.i("Changed", editable.toString());

                }
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        }
        searchTest.addTextChangedListener(tw)

        return root
    }

}