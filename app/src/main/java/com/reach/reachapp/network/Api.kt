package com.reach.reachapp.network

import com.reach.reachapp.models.categories.CategoriesModel
import com.reach.reachapp.models.offers.OffersModel
import io.reactivex.Observable

import retrofit2.http.GET

interface Api {


    @GET("timeline")
    fun getOffersList(): Observable<OffersModel>

    @GET("categories")
    fun getCategories(): Observable<CategoriesModel>

}


